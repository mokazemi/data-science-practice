'''
This Program calculates integral of a polynomial in both analytical and numerical way.
'''
from math import *

#Function definition:

def analyticalInt (cofs,a,b):
	I=0
	c=len(cofs)-1 #It's now equal to degree of polynomial 
	for i in cofs:
		I += i*( b**(c+1)/(c+1) - a**(c+1)/(c+1) )
		c-=1
	return I
	
def polymer(cofs,x): #Such a graceful name! :D
	r=0
	d=len(cofs)
	for i in range(d): 	#From more degree to less
		r += cofs[i]*x**(d-1-i)
	return r

def numInt(cofs,a,b,n):
	I=0
	dx=float(b-a)/n
	for i in range(n):
		I += ( polymer( cofs , a+i*dx )+polymer( cofs , a+(i+1)*dx ))*dx/2
	return I


#Get inputs:
d=input("Enter degree of your polynomial: ")
X=[]
d=int(d)
for i in range(d+1):
	X.append(int(input("x^"+str(d-i)+" coefficient= ")))
a=int(input("Enter Initial point= "))
b=int(input("Enter Final point = "))
n=int(input("How many steps for numerical integration? "))

#Just to be a bit more user-friendly :)
print("-"*20)
print("\n f(x)= ",end='')
for i in range(d):
	print(str(X[i])+"x^"+str(d-i)+" + ",end='')
print(str(X[-1])+"\n")
print("integral of f(x) from "+str(a)+" to "+str(b)+": \n")

#Calculation
print("Numerical=",numInt(X,a,b,n))
print("Analytical=",analyticalInt(X,a,b))
